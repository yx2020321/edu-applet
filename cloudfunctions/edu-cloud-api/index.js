// 教务系统接口请求
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const authUser = require('./authUser/index');
const getCurrentTime = require('./getCurrentTime/index');
const getKbcxAzc = require('./getKbcxAzc/index');
const getCjcx = require('./getCjcx/index');
const getKxJscx = require('./getKxJscx/index');
// 云函数入口函数
exports.main = async (event, context) => {
  let action = event.action;
  /** 结果预处理 */
  let resHandle = event.resHandle || false
  switch(action){
    /**
     * 用户登录获取TOKEN
     */
    case 'authUser':{
      let res =  await authUser.main(event,context);
      return res;
      break
    }
    /**
     * 用户获取当前日期信息
     */
    case 'getCurrentTime':{
      return await getCurrentTime.main(event,context);
      break
    }
    /** 获取一周的课程信息 */
    case 'getKbcxAzc':{
      return await getKbcxAzc.main(event,context);
      break
    }
    case 'getCjcx':{
      return await getCjcx.main(event,context);
      break
    }
    case 'getKxJscx':{
      return await getKxJscx.main(event,context);
      break
    }
  }
}