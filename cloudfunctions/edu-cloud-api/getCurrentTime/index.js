const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const axios = require('axios');
const TimeUtil = require('../timeutil');
const USER = 'edu-cloud-users'
const db = cloud.database()

exports.main = async (event, context) => {
  /**初始化请求接口 */
  var EDU_API = 'http://jwgl.sdust.edu.cn/app.do?method=getCurrentTime&currDate=#{date}'
  /** 获取查询日期 */
  let date = event.date || TimeUtil.TimeCodeYmd()
  /** 替换请求参数 */
  EDU_API = EDU_API.replace('#{date}', date);
  /** 获取OPENID */
  const wxContext = cloud.getWXContext()
  let openid = wxContext.OPENID;
  console.log(openid)
  /** 获取TOKEN */
  let token = '';
  let queryRes = await db.collection(USER).where({
    openid
  }).get();
  if (queryRes.data.length == 0) {
    return {
      errCode: -1,
      errMsg: '未绑定学号信息'
    }
  } else {
    let userBase = queryRes.data[0];
    token = userBase.token;
  }
  let reqRes = await axios({
    method: "GET",
    url: EDU_API,
    headers: {
      "content-type": "application/json;charset=utf-8",
      "User-Agent": "Mozilla/5.0 (Linux; U; Mobile; Android 6.0.1;C107-9 Build/FRF91 )",
      "cache-control": "max-age=0",
      "token": token
    }
  })
  if (reqRes.data.token == -1) {
    //TOKEN 过期
    console.log('--更新TOKEN--')
    let authRes = await cloud.callFunction({
      name: 'edu-token-center',
      data: {
        action: 'flushToken',
        openid: openid
      }
    })
    reqRes = await axios({
      method: "GET",
      url: EDU_API,
      headers: {
        "content-type": "application/json;charset=utf-8",
        "User-Agent": "Mozilla/5.0 (Linux; U; Mobile; Android 6.0.1;C107-9 Build/FRF91 )",
        "cache-control": "max-age=0",
        "token": authRes.result.data.token
      }
    })
  }
  return {
    status: reqRes.status,
    data: reqRes.data
  };
}