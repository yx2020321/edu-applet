const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const axios = require('axios');
const authUserHandler = require('./handler');
const db = cloud.database()

exports.main = async (event, context) => {
  /**初始化请求接口 */
  var EDU_API = 'http://jwgl.sdust.edu.cn/app.do?method=authUser&xh=#{xh}&pwd=#{password}';
  /** 是否对返回结果预处理 */
  let resHandle = event.resHandle || false
  /** 替换请求参数 */
  EDU_API = EDU_API.replace('#{xh}', event.xh);
  EDU_API = EDU_API.replace('#{password}', event.password);
  let reqRes = await axios({
    method: "GET",
    url: EDU_API,
    headers: {
      "content-type": "application/json;charset=utf-8",
      "User-Agent": "Mozilla/5.0 (Linux; U; Mobile; Android 6.0.1;C107-9 Build/FRF91 )",
      "cache-control": "max-age=0"
    }
  })
  let res = {
    status:reqRes.status,
    data:reqRes.data
  }; 
  /** 是否对数据进行预处理 */
  if(resHandle){
    return await authUserHandler.main(res)
  }else{
    return res;
  }
}