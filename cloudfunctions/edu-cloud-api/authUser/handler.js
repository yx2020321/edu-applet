exports.main = async (res) => {
  if(res.status != 200){
    return {
      errCode:-1,
      errMsg:'请求ERROR'
    }
  }
  else if(res.data.flag == '1' || res.data.flag == 1 || res.data.flag == true){
    return {
      errCode:0,
      errMsg:res.data.msg,
      data:res.data
    }
  }else{
    return {
      errCode:-1,
      errMsg:res.data.msg
    }
  }
}