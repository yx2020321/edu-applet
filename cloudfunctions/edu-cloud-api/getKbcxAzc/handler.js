exports.main = async (res) => {
    if (res.status != 200) {
        return {
            errCode: -1,
            errMsg: '请求ERROR'
        }
    } else {
        /**对数据进行预处理 
         * 映射为如下类型数据
        */
        let tableData = res.data;
        // console.log(tableData)
        tableData.map(v=>{
            // console.log(v)
            v.classroom = v.jsmc;
            /**从课程时间拆解重要信息 */
            let dayStr = v.kcsj.substr(0,1)
            // console.log(dayStr)
            v.days = parseInt(dayStr)
            let startTimeStr = v.kcsj.substr(1,2);
            v.nums =  parseInt(startTimeStr)
            let endTimeStr = v.kcsj.substr(3,2)
            v.enum = parseInt(endTimeStr)
            v.subject_id = parseInt(v.kcsj)+Math.round(Math.random()*1000)
            v.sname = v.kcmc
            // console.log(v)
        })
        return tableData;
    }
}
/**
 * courseInfo: [{
            sname:'数据结构',
            classroom:'J7-102',
            days:1,
            nums:2,
            enum:3,
            subject_id:1002
        }],
 */