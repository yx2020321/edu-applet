// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database();
const USER = 'edu-cloud-users'
// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()
    let openid = event.openid || wxContext.OPENID;
    /**获取基础的用户信息 */
    let userRes = await db.collection(USER).where({
      openid
    }).get();
    console.log(userRes);
    let userInfo = null;
    if(userRes.data.length != 0){
      /**如果存在对应用户信息 */
      userInfo = userRes.data[0];
    }
    return {
      userInfo
    }
}