const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
const NOTICE = 'cloud-edu-notice';

exports.main = async (event, context) => {
  /**获取url */
  let noticeUrl = event.url;
  let queryRes = await db.collection(NOTICE).where({
      noticeUrl
  }).get();
  if(queryRes.data.length == 0){
      return {
          errCode:-1,
          errMsg:'该地址不在可解析列表'
      }
  }else{
      let res = queryRes.data[0];
      return res;
  }
}