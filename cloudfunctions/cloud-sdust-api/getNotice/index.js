const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
const NOTICE = 'cloud-edu-notice';
const PAGESIZE = 20;
exports.main = async (event, context) => {
  let page = event.page || 1
  let step = parseInt(page-1) * PAGESIZE;
  let noticeRes = await db.collection(NOTICE).skip(step).limit(PAGESIZE).orderBy('noticeDate','desc').get();
  return noticeRes.data;
}