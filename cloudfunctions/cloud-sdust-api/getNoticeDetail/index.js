const cloud = require('wx-server-sdk')
cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
const axios = require('axios');
const cheerio = require('cheerio');
const request = require('request');
const NOTICE = 'cloud-edu-notice';
const NOTICE_CON = 'cloud-edu-notice-content';
async function execRequest(URL) {
    console.log(URL)
    return new Promise(function (resolve, reject) {
        request({
            method: "GET",
            url: URL,
            encoding: 'UTF-8',
            headers: {
                "Content-Type": "text/html;",
                "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/92.0.4515.131"
            }
        }, function (error, response, body) {
            // console.log(error)
            // console.log(body)
            // console.log(response)
            // var buf =  iconv.decode(body, 'UTF-8');
            // console.log(buf)
            resolve(response.body)
        })
    })
}
async function getReq(url, data) {
    return new Promise(function (resolve, reject) {
        axios({
            method: 'GET',
            url: url,
            data: data,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/92.0.4515.131'
            }
        }).then(response => {
            // console.log(response)
            resolve(response.data)
        })
    })
}
exports.main = async (event, context) => {
    let docId = event.docId;
    /**检索匹配目录是否存在 */
    let notConRes = await db.collection(NOTICE_CON).where({
        noticeId: docId
    }).get();
    if (notConRes.data.length > 0) {
        let res = notConRes.data[0];
        return res;
    } else {
        /**获取通知信息 */
        try {
            let noticeRes = await db.collection(NOTICE).doc(docId).get();
            let noticeData = noticeRes.data;
            /**组装接口 */
            let URL = 'https://www.sdust.edu.cn/' + noticeData.noticeUrl
            let reqData = await execRequest(URL)
            // let reqData = await getReq(URL,{});
            console.log(reqData)
            $ = cheerio.load(reqData);
            $('#div_vote_id').empty();
            let content = $('.pageBox form').html();
            /**写入数据库 */
            await db.collection(NOTICE_CON).add({
                data: {
                    noticeId: noticeData._id,
                    noticeUrl: noticeData.noticeUrl,
                    noticeTitle: noticeData.noticeTitle,
                    noticeContent: content
                }
            })
            return {
                noticeId: noticeData._id,
                noticeUrl: noticeData.noticeUrl,
                noticeTitle: noticeData.noticeTitle,
                noticeContent: content
            };
        } catch (error) {
            console.log(error)
            return {
                errCode: -1,
                errMsg: '没有该条记录对应的信息'
            }
        }
    }
}