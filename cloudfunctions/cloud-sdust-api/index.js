// 教务系统接口请求
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const getNotice = require('./getNotice/index');
const getNoticeDetail = require('./getNoticeDetail/index');
const getNoticeId = require('./getNoticeId/index');
// 云函数入口函数
exports.main = async (event, context) => {
  let action = event.action;

  switch(action){
    case 'getNotice':{
      /**获取公告 */
      return getNotice.main(event,context);
      break
    }
    case 'getNoticeDetail':{
      /**获取公告详细信息 */
      return getNoticeDetail.main(event,context);
      break 
    }
    case 'getNoticeId':{
      return getNoticeId.main(event,context);
      break
    }
  }
}