


![](https://img.shields.io/badge/build-passing-brightgreen) ![](https://img.shields.io/badge/progress-100%25-blue) ![](https://img.shields.io/badge/维护状态-长期维护-green)


**校园小程序(组件版/云开发)**

`@feature`

- [x] 考研政治答题

`@tool`

- [x] 单词检索
- [x] 英汉互译工具
- [x] 单词册 -- 欢迎开发者贡献

