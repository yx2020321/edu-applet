// components/flow-login/index.js
Component({
  options: {
    addGlobalClass: false,
    multipleSlots: true
  },
  /**
   * 组件的属性列表
   */
  properties: {
    avatarUrl:{
      type:String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // loginId:'202001030714',
    // passWord:'LXXLYL01040101'
    loginId:'',
    passWord:''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    toBind(){
      //点击绑定事件
      this.triggerEvent('btnclick',this.data)
    }
  }
})
