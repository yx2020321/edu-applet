// components/edu/grade.js
Component({
  //继承colorui样式
  options: {
    addGlobalClass: false,
    multipleSlots: true
  },
  /**
   * 组件的属性列表
   */
  properties: {
    grades:{
      type:Array,
      value:[],
      observer:function(newVal,oldVal){
        var _  = this;
        _.setData({
          gradeList:newVal
        },()=>{
          this.calcpa();
        })
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    gradeList:[],
    xh: '', //学号
    xnxq: '',
    gxk: 0, //公选课
    bxk: 0, //必修
    xxk: 0, //选修
    cpa: 0, //绩点
    key:'',//检索关键词
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getkey(e){
      this.setData({
        key:e.detail.value
      })
    },
    match(){
      var _ = this;
      var reg = new RegExp(_.data.key,"i");
      var tlist = _.properties.grades;
      var newGradeList = [];
      for(var i=0;i<tlist.length;i++){
        //匹配
        var t = tlist[i];
        if(t.kcmc.match(reg)||t.kclbmc.match(reg)||t.kcxzmc.match(reg)){
          newGradeList.push(t);
        }
      }
      _.setData({
        gradeList:newGradeList
      })
    },
    calcpa(){
      var _ = this;
      var arr = _.data.gradeList;
      var gxk = 0,
        xxk = 0,
        bxk = 0,
        cpa = 0;
      for (var i = 0, len = arr.length; i < len; i++) {
        if (arr[i].kclbmc == '必修') {
          bxk = bxk + arr[i].xf;

        } else if (arr[i].kclbmc == '限选') {
          xxk = xxk + arr[i].xf;
        } else if (arr[i].kclbmc == '公选') {
          gxk = gxk + arr[i].xf;
        }
        if (arr[i].zcj == '优') {
          cpa = cpa + 4.5;
        } else if (arr[i].zcj == '良') {
          cpa = cpa + 3.5
        } else if (arr[i].zcj == '中') {
          cpa = cpa + 2.5;
        } else if (arr[i].zcj == '及格') {
          cpa = cpa + 1;
        } else {
          if (arr[i].zcj == '不及格' || arr[i].zcj < 60) {
            console.log('--不及格--')
          } else {
            cpa = cpa + (arr[i].zcj - 50) / 10;
          }
        }
      }
      _.setData({
        gxk: gxk,
        bxk: bxk,
        xxk: xxk,
        cpa: cpa.toFixed(1),
      })
    }
  }
})
