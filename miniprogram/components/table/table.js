/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const app = getApp();

Component({
    options: {
        addGlobalClass: true,
        multipleSlots: true
    },
    /**
     * 组件的对外属性
     */
    properties: {
        // 行数据
        columns: {
            type: [Object],
            default: [], // top left hybrid
        },
        // 列头数据
        row: {
            type: [Object],
            default: [], // top left hybrid
        },
        // 数据内容,
        dataSource: {
            type: [Object],
            default: [],
        },
        disabled: {
            type: Boolean,
            default: false,
        },
        // 数据内容,
        colorList: {
            type: [Object],
            default: [],
        },
        // 合并
        onAdd: {
            type: Object,
            default: null,
        },
        // 外边框
        bordered: {
            type: Boolean,
            default: false,
        },
        // 加载中
        loading: {
            type: Boolean,
            default: false,
        },
        // 分页器
        pagination: {
            type: [Boolean, Object],
            default: false,
        },
    },
    /**
     * 组件的初始数据
     */
    data: {},
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {},
    ready: function () {
        this.setData({
            columns: [{
                    dataIndex: "name",
                    key: "name",
                    title: "Name",
                },
                {
                    title: "Age",
                    dataIndex: "age",
                    key: "age",
                },
                {
                    title: "Address",
                    dataIndex: "address",
                    key: "address",
                },
                {
                    title: "Tags",
                    key: "tags",
                    dataIndex: "tags",
                },
                {
                    title: "Action",
                    key: "action",
                    dataIndex: "action",
                },
                {
                    title: "Action",
                    key: "action",
                    dataIndex: "action",
                },
                {
                    title: "Action",
                    key: "action",
                    dataIndex: "action",
                },
            ],
            row: [{
                    s_time: "8:00",
                    e_time: "8:50"
                },
                {
                    s_time: "9:00",
                    e_time: "9:50"
                },
                {
                    s_time: "10:10",
                    e_time: "11:00"
                },
                {
                    s_time: "10:10",
                    e_time: "12:00"
                },
                {
                    s_time: "14:00",
                    e_time: "14:50"
                },
                {
                    s_time: "15:00",
                    e_time: "15:50"
                },
                {
                    s_time: "16:00",
                    e_time: "16:50"
                },
                {
                    s_time: "17:00",
                    e_time: "17:50"
                },
                {
                    s_time: "19:00",
                    e_time: "19:50"
                },
                {
                    s_time: "20:00",
                    e_time: "20:50"
                },
            ],
        });
    },
    pageLifetimes: {
        // 组件所在页面的生命周期函数
        show: function () {
            clearTimeout();
            this.setData({
                BlankId: null,
            });
        },
    },
    /**
     * 组件的方法列表
     */
    methods: {
        selectBlank(e) {
            if (this.data.disabled) {
                return;
            }
            let id = e.currentTarget.dataset.id;
            let {
                BlankId
            } = this.data;
            let timer = setTimeout(() => {
                this.setData({
                    BlankId: null,
                });
            }, 3000);
            if (BlankId === id) {
                clearTimeout(timer);
                this.triggerEvent("onAdd", id);
            }
            this.setData({
                BlankId: id,
                editTableId: null,
            });
        },
        editTable(e) {
            let id = e.currentTarget.dataset.id;
            // console.log(id)
            let courseDataList = this.properties.dataSource;
            let courseData = {};
            courseDataList.forEach(function (ele, index) {
                if (ele.subject_id == id) {
                    courseData = ele
                }
            })
            console.log(courseData)
            this.triggerEvent("onView", courseData);
            this.setData({
                // editTableId: id,
                BlankId: null,
            });
        },
    },
});