// components/week-switch/index.js
Component({
    /**
     * 组件的一些选项
     */
    options: {
        addGlobalClass: false,
        multipleSlots: true
    },
    /**组件生命周期 */
    lifetimes:{
        attached(){
            this.courseNum();
        }
    },
    /**
     * 组件的属性列表
     */
    properties: {
        weeksSwitch:{
            type:Boolean,
            value:false
        },
        CustomBar:{
            type:Number
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        checkbox:[],
    },

    /**
     * 组件的方法列表
     */
    methods: {
        ChooseCheckbox(e){
            let value = e.currentTarget.dataset.value;
            /**触发事件 */
            this.triggerEvent('weekchange',value);
            // console.log(e)
        },
        hideweeksSwitch(){
            this.setData({
                weeksSwitch:false
            })
        },
        courseNum() {
            let checkbox = [];
            for (let index = 1; index <= 20; index++) {
                checkbox.push({
                    value: index,
                    checked: false,
                });
            }
            this.setData({
                checkbox,
            });
        },
    }
})