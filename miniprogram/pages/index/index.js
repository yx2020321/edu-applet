const app = getApp();
import config from "../../config";
const dayjs = require('dayjs');
const timeutil = require('timeutil');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    classTime: config.classTime,
    navShow: false,
    StatusBar: null,
    CustomBar: null,
    todayCourse: null
  },
  /**初始化课表 */
  initTable() {
    var that = this;
    /**获取当前星期几 */
    let days = dayjs().day()
    // days = 1
    let hour = dayjs().hour();
    let minute = dayjs().minute();
    let timeStr = timeutil.formatNumber(hour) + ":" + timeutil.formatNumber(minute)
    console.log(timeStr)
    // console.log(days)
    wx.cloud.callFunction({
      name: 'edu-timetable-api',
      data: {
        action: 'initTable'
      },
      success: res => {
        /**获取成功 */
        if (res.result.errCode == -1) {
          /**提示错误信息 */
          wx.showToast({
            title: res.result.errMsg,
            icon: 'none'
          })
          this.setData({
            todayCourse: []
          })
        } else {
          let timetableData = res.result.timetableData;
          let todayCourse = []
          let Index = -1;
          timetableData.forEach(function (ele, index) {
            if (ele.kssj >= timeStr && ele.days == days) {
              todayCourse.push(ele)
            }
          })
          console.log(todayCourse)
          that.setData({
            todayCourse
          })

        }
      },
      complete: res => {
        console.log(res)
      }
    })
  },
  /**
   * @Course-Card 组件绑定映射函数
   */
  toLogin() {
    console.log('页面跳转')
    wx.navigateTo({
      url: '/pages/ulogin/index',
    })
  },
  /**
   * @Plugin-Nav 组件绑定映射函数
   */
  wordSelect(e) {
    let word = e.detail.word;
    wx.navigateTo({
      url: 'plugin://edict-plugin/word?word=' + word,
    })
  },
  /**
   * 页面路由函数
   */
  tapEvent(e) {
    // this.setData({
    //   navShow:false
    // })
    let Index = e.currentTarget.dataset.index;
    let Key = e.currentTarget.dataset.key;
    // console.log(Index)
    switch (Key) {
      case '考研政治': {
        wx.navigateToMiniProgram({
          appId: 'wx501a84fb75239d2e',
        })
        break
      }
      case '单词检索': {
        this.setData({
          navShow: true
        })
        break
      }
      case '空教室': {
        wx.navigateTo({
          url: '/pages/edu/empty/index',
        })
        break
      }
      case '英汉互译': {
        let url = 'plugin://edict-plugin/en-zh'
        wx.navigateTo({
          url: url,
        })
        break
      }
      case '单词册': {
        let openid = app.globalData.openid
        let url = 'plugin://edict-plugin/index?userId=' + openid
        wx.navigateTo({
          url: url,
        })
        break
      }
      case '课表': {
        let openid = app.globalData.openid;
        wx.navigateTo({
          url: '/pages/edu/timetable/index',
        })
        break
      }
      case '图书检索': {
        /**跳转到图书详情页 */
        wx.navigateTo({
          url: '../booklib/index',
        })
        break
      }
      case '成绩': {
        /**跳转到成绩详情页 */
        wx.navigateTo({
          url: '../edu/score/index',
        })
        break
      }
      case '学校公告': {
        wx.navigateTo({
          url: '../sdust/notice/index',
        })
        break
      }
      case '个人信息': {
        wx.navigateTo({
          url: '/pages/setting/index',
        })
        break
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      CustomBar: app.globalData.CustomBar,
      StatusBar: app.globalData.StatusBar
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.todayCourse != null) {
      console.log('跳过请求今日课表')
    } else {
      this.initTable();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})