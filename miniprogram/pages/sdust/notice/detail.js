// pages/sdust/notice/detail.js
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        content: '',
        vis:true
    },
    /**组件点击标签 */
    wxmlTagATap(e) {
        console.log(e)
        /**判断是文件还是网址 */
        let url = e.detail.src;
        if (url.indexOf('.htm') != -1) {
            url = 'info/1145/' + url
            console.log(url)
            /**查询对应ID */
            wx.showLoading({
                title: '解析查询',
            })
            wx.cloud.callFunction({
                name: 'cloud-sdust-api',
                data: {
                    action: 'getNoticeId',
                    url
                },
                success: res => {
                    console.log(res)
                    if (res.result.errCode == -1) {
                        this.setData({
                            errMsg: res.result.errMsg
                        })
                    } else {
                        let docId = res.result._id;
                        wx.redirectTo({
                            url: '/pages/sdust/notice/detail?docId=' + docId,
                        })
                    }

                },
                complete: res => {
                    wx.hideLoading()
                }
            })
        } else if (url.indexOf('download.jsp') != -1) {
            console.log('文件下载')
            url = 'https://www.sdust.edu.cn' + url
            /**下载文件 */
            wx.showLoading({
                title: '下载',
            })
            wx.cloud.callFunction({
                name: 'cloud-file-loader',
                data: {
                    url
                },
                success: res => {
                    console.log(res)
                    let fileID = res.result.fileID;
                    wx.cloud.downloadFile({
                        fileID: fileID,
                        success: ans => {
                            console.log(ans)
                            wx.openDocument({
                                filePath: ans.tempFilePath,
                            })
                        }
                    })
                },
                fail: res => {
                    this.setData({
                        errMsg: '文件下载失败'
                    })
                    console.log(res)
                },
                complete: res => {
                    wx.hideLoading();
                }
            })
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.showLoading({
            title: '获取详细内容中',
        })
        let docId = options.docId || "cd045e756130d8e40a213b8d1fb539f9"
        this.setData({
            docId
        })
        wx.cloud.callFunction({
            name: 'cloud-sdust-api',
            data: {
                action: 'getNoticeDetail',
                docId
            },
            success: res => {
                console.log(res)
                this.setData({
                    vis:false,
                    content: res.result.noticeContent
                })
            },
            fail:res=>{
                wx.showToast({
                  title: '网络错误',
                  icon:'none'
                })
            },
            complete: res => {
                wx.hideLoading();

            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return {
            title: `学校公告详情`,
            path: 'pages/sdust/notice/detail?docId='+this.data.docId,
        }

    }
})