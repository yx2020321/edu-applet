// pages/sdust/notice/index.js
const cwx = require('../../../localfunctions/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    /**通知列表 */
    noticeList: [],
    height: ''
  },
  /**初始化高度 */
  initHeight() {
    let that = this;
    // 获取系统信息
    wx.getSystemInfo({
      success: function (res) {
        // 获取可使用窗口宽度
        let clientHeight = res.windowHeight;
        // 获取可使用窗口高度
        let clientWidth = res.windowWidth;
        // 算出比例
        let ratio = 750 / clientWidth;
        // 算出高度(单位rpx)
        let height = clientHeight * ratio;
        // 设置高度
        that.setData({
          height: height
        });
      }
    })
  },
  /**点击事件 */
  tapEvent(e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/sdust/notice/detail?docId=' + id,
    })
  },
  /**请求通知信息 */
  reqNotice() {
    var that = this;
    let noticeList = that.data.noticeList;
    if (noticeList.length % 20 != 0) {
      wx.showToast({
        title: '没有更多了',
        icon: 'none'
      })
    } else {
      let page = noticeList.length / 20 + 1
      wx.showLoading({
        title: '获取最新通知~',
      })
      cwx.main({
        action: 'getNotice',
        page
      }).then(res => {
        // console.log(res)
        wx.hideLoading();
        that.setData({
          noticeList: noticeList.concat(res)
        })
      })
      //   wx.cloud.callFunction({
      //     name:'cloud-sdust-api',
      //     data:{
      //       action:'getNotice',
      //       page:page
      //     },
      //     success:res=>{
      //       console.log(res)
      //       that.setData({
      //         noticeList:noticeList.concat(res.result)
      //       })

      //     },
      //     fail:res=>{
      //       console.log(res)
      //     },
      //     complete:res=>{
      //       // console.log(res)
      //       wx.hideLoading();
      //     }
      //   })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.reqNotice();

    this.initHeight();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.reqNotice();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: `山科小程序-学校公告`,
      path: 'pages/sdust/notice/index'
    }
  }
})