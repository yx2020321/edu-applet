const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    displayArea: app.globalData.displayArea,
    userInfo: null,
    //可选列表
    settingList: [{
        name: "情侣课表",
        icon: "/static/edu/couples.png",
        url: "/pages/edu/timetable/couples?",
      },
    ],
  },
  /**返回首页 */
  backIndex(){
    var allpages = getCurrentPages();
    let pagelen = allpages.length;
    if(pagelen == 1){
      wx.reLaunch({
        url: '/pages/index/index',
      })
    }else{
      wx.navigateBack({
        delta: 0,
      })
    }
    // console.log(allpages.length)
    
  },
  navigationTo(e){
    console.log(e)
    let url = e.currentTarget.dataset.url;
    let userInfo = this.data.userInfo;
    if(!userInfo){
      wx.showToast({
        title: '请先登录',
        icon:'none'
      })
    }else{
      wx.navigateTo({
        url: url,
      })
    }
    
  },
  /**点击跳转登录页面 */
  tapLogin() {
    wx.navigateTo({
      url: '/pages/ulogin/index',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    /**调用接口身份验证 */
    var that = this;
    wx.showLoading({
      title: '加载中',
      mask:true
    })
    wx.cloud.callFunction({
      name: 'cloud-user-setting',
      complete: res => {  
        console.log(res)
        wx.hideLoading();
        that.setData({
          userInfo: res.result.userInfo
        })
        wx.setStorageSync('userInfo', res.result.userInfo)
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})