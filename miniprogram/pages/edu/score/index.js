// pages/edu/score/index.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    gradeList: []
  },
  reqGrade() {
    wx.showLoading({
      title: '请求成绩~',
    })
    wx.cloud.callFunction({
      name: 'edu-cloud-api',
      data: {
        action: 'getCjcx'
      },
      success: res => {
        if (res.result.errCode == -1) {
          this.setData({
            errMsg: res.result.errMsg
          })
          setTimeout(function () {
            wx.showModal({
              title: '提示',
              content: '是否跳转到登录页面',
              success: r => {
                if (r.confirm) {
                  wx.navigateTo({
                    url: '/pages/ulogin/index',
                  })
                } else if (r.cancel) {
                  wx.navigateBack()
                }
              }
            })
          }, 1000)
        } else {
          this.setData({
            gradeList: res.result.data
          })
        }
      },
      fail:res=>{
        wx.showToast({
          title: '网络错误',
          icon:'none'
        })
      },
      complete: res => {
        console.log(res)
        wx.hideLoading();
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.reqGrade();
    app.emitter.on('bindSucc', r => {
      this.reqGrade();
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: `山科小程序-成绩`,
      path: 'pages/edu/score/index',
      imageUrl: '/static/setting/coursenull.png'
    }
  }
})