// pages/edu/timetable/couples.js
const app = getApp();
Page({
    InitCustom() {
        app.InitCustom();
        this.setData({
            StatusBar: app.globalData.StatusBar,
            CustomBar: app.globalData.CustomBar,
            displayArea: app.globalData.displayArea,
        })
    },
    /**
     * 页面的初始数据
     */
    data: {
        coupleId: '',
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        displayArea: app.globalData.displayArea,
        //历史留言
        // messageList: [{
        //     time: '2021-09-10',
        //     contents: '木头人'
        // }]
        messageList:[],
    },
    /**返回上一些 */
    BackPage() {
        var allpages = getCurrentPages();
        let pagelen = allpages.length;
        if (pagelen == 1) {
            wx.reLaunch({
                url: '/pages/index/index',
            })
        } else {
            wx.navigateBack({
                delta: 0,
            })
        }

    },
    /**解除情侣关联 */
    removeBind() {
        var that = this;
        wx.showModal({
            title: '提醒',
            content: `是否确认和${that.data.otherBase.realName}解除情侣关系`,
            success: r => {
                if (r.confirm) {
                    wx.showLoading({
                        title: '解除中~',
                        mask: true
                    })
                    wx.cloud.callFunction({
                        name: 'edu-couples-api',
                        data: {
                            action: 'unlinkCouple'
                        },
                        success: res => {
                            that.setData({
                                coupleId: ''
                            })
                        },
                        complete: res => {
                            wx.hideLoading();
                        }
                    })
                } else if (r.cancel) {
                    wx.showToast({
                        title: '取消操作',
                        icon: 'none'
                    })
                }
            }
        })

    },
    /**请求情侣信息 */
    reqCouple() {
        var that = this;
        wx.showLoading({
            title: '获取情侣信息',
        })
        wx.cloud.callFunction({
            name: 'edu-couples-api',
            data: {
                action: 'findCouple'
            },
            success: res => {
                that.setData({
                    userBase: res.result.userBase,
                    otherBase: res.result.otherBase,
                    coupleId: res.result.coupleId
                })
            },
            complete: res => {
                console.log(res)
                wx.hideLoading();
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        /**获取绑定信息 */
        let bindId = options.bindId;
        if (bindId) {
            wx.showLoading({
                title: '加载中',
            })
            // 如果绑定ID 不为空
            wx.cloud.callFunction({
                name: 'cloud-user-api',
                data: {
                    action: 'findById',
                    docId: bindId
                },
                success: res => {
                    wx.hideLoading();
                    console.log(res)
                    if (res.result.errCode == -1) {
                        that.setData({
                            errMsg: res.result.errMsg
                        })
                    } else {
                        wx.showModal({
                            title: '确认提醒',
                            content: `是否要和${res.result.data.realName}绑定情侣关系`,
                            success: r => {
                                if (r.confirm) {
                                    wx.showLoading({
                                        title: '绑定中',
                                    })
                                    /**确认绑定 */
                                    /**
                                     * 1. 确认双方 -- 避免重复绑定
                                     * 2. 表单校验 -- 需要绑定学号的用户可以
                                     */
                                    wx.cloud.callFunction({
                                        name: 'edu-couples-api',
                                        data: {
                                            bindId,
                                            action: 'bindCouple'
                                        },
                                        success: ans => {
                                            if (ans.result.errCode == -1) {
                                                that.setData({
                                                    errMsg: ans.result.errMsg
                                                })
                                            } else {
                                                that.setData({
                                                    succMsg: '绑定成功'
                                                })
                                            }
                                            that.reqCouple();
                                        },
                                        complete: ans => {
                                            wx.hideLoading();
                                        }
                                    })
                                } else {
                                    wx.showToast({
                                        title: '您取消了绑定',
                                        icon: 'none'
                                    })
                                }
                            }
                        })
                    }

                },
                complete: res => {

                }
            })
        } else {
            that.reqCouple();
        }
        this.InitCustom();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        let userInfo = wx.getStorageSync('userInfo')
        // console.log(userInfo)
        let realName = userInfo.realName;
        return {
            title: `${realName}请求和你绑定为情侣`,
            path: '/pages/edu/timetable/couples?bindId=' + userInfo._id,
            imageUrl: '/static/setting/couples.png'
        }
    }
})