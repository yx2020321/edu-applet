// pages/edu/timetable/edit.js
const db = wx.cloud.database();
import config from "../../../config";
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        days_cn :['一','二','三','四','五','六','日'],
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        displayArea: app.globalData.displayArea,
        //待输入元素
        //课程
        sname: '',
        //教室
        classroom:'',
        //教师名称
        jsxm:'',
        checkbox:[],
        //当前周几
        days:1,
        nums:0,
        enum:0,
        currday:0,
        //已选择周次
        checkeds:[],
        editId:null,
        customCourse:[]
    },
    /**清空输入信息 */
    emptyCourse(){
        var that = this;
        that.setData({
            sname:'',
            classroom:'',
            jsxm:'',
            checkeds:[]
        })
    },
    /**删除自定义课表 */
    delCourse(e){
        var that = this;
        // console.log(e)
        let item = e.currentTarget.dataset.item;
        let index = e.currentTarget.dataset.index;
        console.log(index)
        console.log(item)
        // 获取ID
        let docId = item._id;
        wx.showLoading({
          title: '删除中',
        })
        wx.cloud.callFunction({
            name:'edu-timetable-api',
            data:{
                action:'deleteCustom',
                docId
            },
            success:res=>{
                console.log(res)
                let customCourse = this.data.customCourse;
                 customCourse.splice(index,1)
                that.setData({
                    customCourse,
                    succMsg:'删除成功'
                },()=>{
                    if(customCourse.length == 0){
                        that.hideModal();
                    }
                })
            },
            complete:res=>{
                wx.hideLoading();
            }
        })
    },
    /**选择课表 */
    selCourse(){
        //基于原条件进行修改
        wx.showToast({
          title: '功能Building~',
          icon:'none'
        })
    },
    /**管理自定义课表 */
    manCourse(){
        var that = this;
        wx.showLoading({
          title: '查询课表',
        })
        /**调用接口查询自定义的课表 */
        wx.cloud.callFunction({
            name:'edu-timetable-api',
            data:{
                action:'queryCustomTable'
            },
            complete:res=>{
                console.log(res)
                if(res.result.length == 0){
                    that.setData({
                        errMsg:'暂无自定义课表'
                    })
                }else{
                    that.setData({
                        modalName:'manModal',
                        customCourse:res.result,
                        show:true
                    })
                }
                
                wx.hideLoading();
            }
        })
    },
    saveButton(){
        var that = this;
        /**保存当前课程信息 */
        let insertFlag = true;
        wx.showLoading({
          title: '写入自定义课表中',
        })
        wx.cloud.callFunction({
            name:'edu-timetable-api',
            data:{
                action:'addTable',
                weeks:that.data.checkeds,
                days:that.data.currday,
                nums:that.data.nums,
                enum:that.data.enum,
                jsxm:that.data.jsxm,
                sname:that.data.sname,
                classroom:that.data.classroom,
                kssj:config.classTime[that.data.nums].s_time
            },
            success:res=>{
                if(res.result.errCode == -1){
                    this.setData({
                        errMsg:res.result.errMsg
                    })
                }else{
                    this.setData({
                        succMsg:'新增课程成功'
                    })
                }
                
            },
            complete:res=>{
                console.log(res)
                wx.hideLoading();
            }
        })
    },
    ChooseCheckbox(e) {
        let items = this.data.checkbox;
        let values = e.currentTarget.dataset.value;
        for (let i = 0, lenI = items.length; i < lenI; ++i) {
            if (items[i].value == values) {
                items[i].checked = !items[i].checked;
                break;
            }
        }
        this.setData({
            checkbox: items,
        });
    },
     /**
     * 保存课程周数
     */
    saveWeek() {
        let { checkbox, editId } = this.data;
        let checkeds = checkbox.map((v) => {
            if (v.checked) {
                return v.value;
            }
        });
        checkeds = checkeds.filter((v) => v);
        console.log(checkeds);
        this.setData({
            checkeds
        })
        // this.modifyData("attend", editId, checkeds.join(","));
        this.hideModal();
    },
   
    /**
     * 选择周数
     */
    ChooseWeeks(e) {
        let value = e.currentTarget.dataset.value;
        let checkbox = this.data.checkbox;
        console.log(value);
        const WeekFunc = (type) => {
            return checkbox.map((v) => {
                type == 0
                    ? (v.checked = true)
                    : type == 1
                        ? (v.checked = v.value % 2 != 0)
                        : (v.checked = v.value % 2 == 0);
                return v;
            });
        };
        let _temp = WeekFunc(value);
        console.log(_temp, "_temp");
        this.setData({
            ChooseWeek: value,
            checkbox: _temp,
        });
    },
    courseNum() {
        let checkbox = [];
        for (let index = 1; index <= 20; index++) {
            checkbox.push({
                value: index,
                checked: false,
            });
        }
        this.setData({
            checkbox,
        });
    },
     /**
     * 隐藏弹窗
     */
    hideModal() {
        this.setData({
            modalName: null,
            editId: null,
        });
    },
    /**展示弹窗 */
    showModal(e){
        let { index, target } = e.currentTarget.dataset;
        this.setData({
            modalName: target,
            editId: index,
        });
    },

    /**返回 */
    BackPage(){
        wx.navigateBack()
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options)
        let days_cn = ['一','二','三','四','五','六','日']
        this.setData({
            days:days_cn[options.day],
            currday:parseInt(options.day)+1,
            nums:parseInt(options.nums)+1,
            enum:parseInt(options.nums)+2
        })
        this.courseNum();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})