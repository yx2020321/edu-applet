// pages/edu/empty/index.js
const app = getApp();
const dayjs = require('dayjs');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    locations: [{
        text: '青岛校区',
        value: '青岛校区'
      },
      {
        text: '泰安校区',
        value: '泰安校区'
      },
      {
        text: '济南校区',
        value: '济南校区'
      },
    ],
    idleTimes: [{
        text: '全天',
        value: 'all'
      },
      {
        text: '上午',
        value: 'am'
      },
      {
        text: '下午',
        value: 'pm'
      },
      {
        text: '晚上',
        value: 'night'
      },
    ],
    location: '青岛校区',
    idleTime: 'all',
    /**根据返回结果生成的选择项目 */
    jxlList: [{
      text: '全部',
      value: '全部'
    }],
    jxl: '全部',
    //空闲列表
    emptyList: [],
    //展示列表
    showList: [],
    //被限制列表
    limitList: [],
    //用户登录状态
  },
  /**修改时间 */
  editIdle(e){
    console.log(e)
    this.setData({
      idleTime:e.detail
    },()=>{
      this.reqEmpty();
    })
  },
  /**修改校区 */
  editLoaction(e){
    this.setData({
      location:e.detail
    },()=>{
      this.groupData();
    })
  },
  /**修改教学楼 */
  editJxl(e){
    console.log(e)
    this.setData({
      jxl:e.detail
    },()=>{
      this.groupData();
    })
  },
  /**根据返回结果整理数据 */
  groupData() {
    let jxlList = [{
      text: '全部',
      value: '全部'
    }]
    let emptyList = this.data.emptyList;
    let location = this.data.location;
    let jxl = this.data.jxl;
    console.log(jxl)
    var jxl_set = new Set();
    let showList = [];
    let limitList = [];
    emptyList.forEach(function (ele, index) {
      if (ele.xqmc == location) {
        jxl_set.add(ele.jzwmc)
        showList.push(ele)
        if (jxl == ele.jzwmc) {
          limitList.push(ele)
        }
      }
    })
    console.log(limitList)
    let jxl_arr = Array.from(jxl_set);
    jxl_arr = jxl_arr.sort(function (a, b) {
      if(a.length == b.length){
        return a > b?1:-1
      }else{
        return a.length > b.length?1:-1
      }
    })
    let _jxl = []
    jxl_arr.forEach(function (ele, index) {
      let obj = {
        text: ele,
        value: ele
      }
      _jxl.push(obj)
    })
    if (limitList.length != 0) {
      console.log('待筛选不为0')
      showList = limitList
      this.setData({
        jxl,
        showList,
        jxlList: jxlList.concat(_jxl)
      })
    } else {
      this.setData({
        showList,
        jxl: '全部',
        jxlList: jxlList.concat(_jxl)
      })
    }
  },
  /**请求获取当前空闲教室 */
  reqEmpty() {
    var that = this;
    wx.showLoading({
      title: '获取空闲教室',
    })
    wx.cloud.callFunction({
      name: 'edu-empty-api',
      data: {
        action: 'getEmpty',
        idleTime: that.data.idleTime,
      },
      complete: res => {
        wx.hideLoading();
        console.log(res)
        if(res.result.errCode == -1){
          that.setData({
            errMsg:res.result.errMsg
          })
          setTimeout(function(){
            wx.showModal({
              title:'提示',
              content:'是否跳转到登录页面',
              success:r=>{
                if(r.confirm){
                  wx.navigateTo({
                    url: '/pages/ulogin/index',
                  })
                }else if(r.cancel){
                  wx.navigateBack()
                }
              }
            })
          },1000)
        }else{
          that.setData({
            emptyList: res.result
          }, () => {
            this.groupData();
          })
        }
      
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    this.reqEmpty();
    app.emitter.on('bindSucc',r=>{
      setTimeout(function(){
        that.reqEmpty();
      },1000)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: `山科小程序-空教室`,
      path: 'pages/edu/empty/index'
  }
  }
})