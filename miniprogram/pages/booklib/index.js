// pages/booklib/index.js
const app = getApp();
const db = wx.cloud.database();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchKey: '',
    history: ['Java'],
    books: []
  },
  /**点击事件*/
  bindViewTap(e) {
    console.log(e)
    let bookno = e.currentTarget.dataset.no;
    let bookItem = e.currentTarget.dataset.item;
    /**调用缓存 */
    wx.setStorageSync('bookItem', bookItem)
    wx.navigateTo({
      url: 'detail?bookno=' + bookno,
    })
  },
  /**请求图书列表 */
  reqBook() {
    var that = this;
    let bookLen = that.data.books.length;
    if (bookLen % 10 != 0) {
      wx.showToast({
        title: '没有更多了~',
        icon: 'none'
      })
    } else {
      let page = bookLen / 10 + 1
      /**调用请求接口 */
      let query = this.data.searchKey;
      /**对缓存信息进行修改 */
      let hisSearch = wx.getStorageSync('hisSearch') || []
      hisSearch.unshift(query)
      hisSearch.splice(4)
      wx.setStorageSync('hisSearch', hisSearch)
      /**调用接口 */
      wx.showLoading({
        title: '获取查询结果',
      })
      wx.cloud.callFunction({
        name: 'cloud-booklib-api',
        data: {
          query,
          page
        },
        success: res => {
          console.log(res)
          that.setData({
            books: that.data.books.concat(res.result)
          })
        },
        complete: res => {
          wx.hideLoading();
        }
      })
    }

  },
  /**快速检索 */
  quickSearch(e) {
    console.log(e)
    let word = e.currentTarget.dataset.keywords;
    this.setData({
      searchKey: word
    }, () => {
      this.reqBook()
    })
  },
  /**调用查询 */
  onSearch(e) {
    console.log(e)
    let searchKey = e.detail;
    this.setData({
      books: [],
      searchKey
    }, () => {
      /**调用请求接口 */
      this.reqBook();
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    /**调用接口，获取最近热搜 */
    const cwx = require('../../localfunctions/index')
    cwx.main({
      action: 'getBookLog'
    }).then(res => {
      console.log(res)
      let tArr = res.data
      let hot = []
      tArr.forEach(function (ele, index) {
        hot.push(ele.keyWord)
      })
      this.setData({
        hot
      })
    })
    // wx.cloud.callFunction({
    //   name:'cloud-log-api',
    //   data:{
    //     action:'findTop'
    //   },
    //   success:res=>{
    //     console.log(res.result)
    //     let tArr = res.result.data
    //     let hot = []
    //     tArr.forEach(function(ele,index){
    //       hot.push(ele.keyWord)
    //     })
    //     this.setData({
    //       hot
    //     })
    //   }
    // })
    /**获取缓存查询历史 */
    let hisSearch = wx.getStorageSync('hisSearch') || []
    this.setData({
      history: hisSearch
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.reqBook();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: `山科小程序-图书检索`,
      path: 'pages/booklib/index'
    }
  }
})