// pages/booklib/detail.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bookNo: '',
    content: '',
    bookItem: {},
    storeList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    /**获取选择 */
    let bookItem = wx.getStorageSync('bookItem')
    this.setData({
      bookNo: options.bookno,
      bookItem
    }, () => {
      this.reqBook();
    })
  },
  /**请求图书详情信息 */
  reqBook() {
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    wx.cloud.callFunction({
      name: 'cloud-book-api',
      data: {
        bookNo: that.data.bookNo
      },
      success: res => {
        console.log(res)
        that.setData({
          storeList: res.result
        })
      },
      complete: res => {
        // console.log(res)
        wx.hideLoading();
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: `山科小程序-图书检索`,
      path: 'pages/booklib/index'
    }
  }
})