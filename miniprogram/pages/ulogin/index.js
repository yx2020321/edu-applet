// pages/ulogin/index.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },
  /**
   * 学号绑定函数
   */
  bindXh(e){
    wx.getUserProfile({
      desc: '获取头像和昵称',
      success: res => {
        let userInfo = res.userInfo
        wx.showLoading({
          title: '登录',
          mask:true
        })
        wx.cloud.callFunction({
          name:'cloud-user-api',
          data:{
            action:'userBind',
            userInfo:userInfo,
            bindId:e.detail.loginId,
            bindPwd:e.detail.passWord
          },
          complete:res=>{
            wx.hideLoading();
            console.log(res)
            if(res.result.errCode == 0){
              this.setData({
                succMsg:res.result.errMsg
              },()=>{
                app.emitter.emit('bindSucc')
                //绑定成功，返回首页
                setTimeout(function(){
                  wx.navigateBack({
                    delta: 0,
                  })
                },1500)
              })
            }else{
              this.setData({
                errMsg:res.result.errMsg
              })
            }
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})