export default {
    CloudID: 'dev-tool-0gx69s0m12d6add1',
    color: {
        primary: "#5E99FB",
    },
    shareText: "大学生都在用的课程表小程序", //分享小程序时候的分享语
    colorList: [
        "#08D8D1",
        "#FFD258",
        "#FF8771",
        "#5E99FB",
        "#12DB7E",
        "#9069F8",
        "#FA82DA",
        "#FFAE5D",
    ],
    classTime: [{
            s_time: "8:00",
            e_time: "8:50"
        },
        {
            s_time: "9:00",
            e_time: "9:50"
        },
        {
            s_time: "10:10",
            e_time: "11:00"
        },
        {
            s_time: "10:10",
            e_time: "12:00"
        },
        {
            s_time: "14:00",
            e_time: "14:50"
        },
        {
            s_time: "15:00",
            e_time: "15:50"
        },
        {
            s_time: "16:00",
            e_time: "16:50"
        },
        {
            s_time: "17:00",
            e_time: "17:50"
        },
        {
            s_time: "19:00",
            e_time: "19:50"
        },
        {
            s_time: "20:00",
            e_time: "20:50"
        },
    ],
};