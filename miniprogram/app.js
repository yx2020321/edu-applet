//app.js
import config from "./config";
import mitt from 'mitt';
const emitter = mitt();
App({
  emitter,
  onLaunch: function () {
    this.InitCloud(); //初始化云服务 / ESC
    this.InitCustom(); //初始化 custom所需配置信息
  },
  InitCloud() {
    var that = this;
    wx.cloud.init({
      env: config.CloudID,
      traceUser: true
    })
    wx.cloud.callFunction({
      name: 'openapi',
      data: {
        action: 'getOpenData'
      },
      success: res => {
        // console.log(res)
        this.globalData.openid = res.result.openid
      }
    })
  },
  InitCustom() {
    console.log('调用窗口计算')
    wx.getSystemInfo({
      success: e => {
        //console.log(e)
        this.globalData.StatusBar = e.statusBarHeight;
        let custom = wx.getMenuButtonBoundingClientRect();
        // console.log(custom)
        this.globalData.Custom = custom;
        this.globalData.CustomBar = custom.bottom + custom.top - e.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.displayArea = {
            windowHeight: e.windowHeight,
            windowWidth: e.windowWidth,
            screenHeight: e.screenHeight,
          };
          this.globalData.Custom = capsule;
          this.globalData.CustomBar =
            capsule.bottom + capsule.top - e.statusBarHeight;
        } else {
          this.globalData.CustomBar = e.statusBarHeight + 50;
        }
      }
    })
  },
  globalData: {}
})