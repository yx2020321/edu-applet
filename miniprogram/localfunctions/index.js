const app = getApp();
const db = wx.cloud.database();
const getNotice = require('./getNotice/index');
const getBookLog = require('./getBookLog/index');
async function main(data){
    let action = data.action;
    switch(action){
        case 'getNotice':{
            return await getNotice.main(data);
            break
        }
        case 'getBookLog':{
            return await getBookLog.main(data);
            break
        }
    }
}
module.exports = {
    main
}