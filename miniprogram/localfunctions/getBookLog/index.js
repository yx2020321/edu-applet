const db = wx.cloud.database()
const NOTICE = 'cloud-edu-notice';
const PAGESIZE = 20;
const LOG = 'cloud-book-log';
exports.main = async (data) => {
    return await db.collection(LOG).limit(10).orderBy('searchTime','desc').get();
}