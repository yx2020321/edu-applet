const db = wx.cloud.database()
const NOTICE = 'cloud-edu-notice';
const PAGESIZE = 20;
exports.main = async (data) => {
    let page = data.page || 1
    let step = parseInt(page - 1) * PAGESIZE;
    let noticeRes = await db.collection(NOTICE).skip(step).limit(PAGESIZE).orderBy('noticeDate', 'desc').get();
    return noticeRes.data;
}